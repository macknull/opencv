#pragma once
#include "stdafx.h"'
#include <opencv2/opencv.hpp>;
#include "AsciiOutput.h"
//using namespace cv;

class VideoToAsciiConverter
{
private:
	std::string inputFilename;
	std::string outputFilename;

	int subdivide(const cv::Mat img, const int scale, std::vector<cv::Mat> &blocks, const int colBaseDivisor, const int rowBaseDivisor);
	int gcd(int a, int b);
	int lcm(int a, int b);

	int exportVideo(
		cv::VideoCapture inputVideo,
		cv::VideoWriter outputVideo,
		int scale,
		int colBaseDivisor,
		int rowBaseDivisor
		);

public:
	VideoToAsciiConverter(std::string inputFilename, std::string outputFilaname);
	~VideoToAsciiConverter();
	void convert();
};


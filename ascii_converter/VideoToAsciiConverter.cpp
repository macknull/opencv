#include "stdafx.h"

#include "VideoToAsciiConverter.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <algorithm>
#include "AsciiOutput.h"

using namespace cv;
using namespace std;

cv::Scalar calculateAvgChannels(cv::Mat frame) {
	cv::Scalar avgChannels = mean(frame);

	double avgBrightness = (avgChannels[0] + avgChannels[1] + avgChannels[2]) / (double)3;

	avgChannels[3] = avgBrightness;
	return avgChannels;
}

int VideoToAsciiConverter::subdivide(const Mat img, const int scale, std::vector<Mat> &blocks, const int colBaseDivisor, const int rowBaseDivisor)
{
	const int colDivisor = colBaseDivisor * scale,
		rowDivisor = rowBaseDivisor * scale;

	/* Checking if the image was passed correctly */
	if (!img.data || img.empty())
		std::cerr << "Problem Loading Image" << std::endl;

	// check if divisors fit to image dimensions
	if (img.cols % colDivisor == 0 && img.rows % rowDivisor == 0)
	{
		for (int y = 0; y < img.cols; y += img.cols / colDivisor)
		{
			for (int x = 0; x < img.rows; x += img.rows / rowDivisor)
			{
				blocks.push_back(img(cv::Rect(y, x, (img.cols / colDivisor), (img.rows / rowDivisor))).clone());																																			//waitKey(0); // visualization
			}
		}
	}
	else if (img.cols % colDivisor != 0)
	{
		cerr << "Error: Please use another divisor for the column split." << endl;
		exit(1);
	}
	else if (img.rows % rowDivisor != 0)
	{
		cerr << "Error: Please use another divisor for the row split." << endl;
		exit(1);
	}
	return -1;
}

int VideoToAsciiConverter::gcd(int a, int b) {
	return b == 0 ? a : gcd(b, a % b);
}

int VideoToAsciiConverter::lcm(int a, int b)
{
	for (int i = 2; i <= min(a, b); i++)
	{
		if ((a % i == 0) && (b%i == 0))
			return i;
	}
	return 1;
}

int VideoToAsciiConverter::exportVideo(
	cv::VideoCapture inputVideo, 
	cv::VideoWriter outputVideo, 
	int scale, 
	int colBaseDivisor,
	int rowBaseDivisor
	)
{
	int frameNum = -1;
	Mat videoFrame,
		image;
	for (;;)
	{
		std::vector<Mat> frameSegments;
		std::vector<cv::Scalar> frameSegmentsAvgChannels;
		int conversionProgress;
		int currentFrame,
			totalFrameCount;

		totalFrameCount = inputVideo.get(CAP_PROP_FRAME_COUNT);
		currentFrame = inputVideo.get(CAP_PROP_POS_FRAMES);

		inputVideo >> videoFrame;

		if (videoFrame.empty())
		{
			cout << "Conversion finished." << endl;
			break;
		}

		++frameNum;

		conversionProgress = (int)((double)currentFrame / (double)totalFrameCount * (double)100);

		image = Mat::zeros(Size(videoFrame.cols, videoFrame.rows), videoFrame.type());

		subdivide(videoFrame, scale, frameSegments, colBaseDivisor, rowBaseDivisor);
		frameSegmentsAvgChannels.resize(frameSegments.size());
		std::transform(frameSegments.begin(), frameSegments.end(), frameSegmentsAvgChannels.begin(), calculateAvgChannels);

		AsciiOutput Output(image, rowBaseDivisor*scale, frameSegments.begin()->cols, colBaseDivisor*scale, frameSegments.begin()->rows, scale);
		Output.makeFrame(frameSegmentsAvgChannels);
		imshow("Display window", image);
		outputVideo.write(image);

		char c = (char)waitKey(35);
		if (c == 27) break;
	}
	cv::destroyWindow("Display window");
	outputVideo.release();

	return 1;
}

VideoToAsciiConverter::VideoToAsciiConverter(std::string inputFilename, std::string outputFilename)
{
	this->inputFilename = inputFilename;
	this->outputFilename = outputFilename;
}

VideoToAsciiConverter::~VideoToAsciiConverter()
{

}

void VideoToAsciiConverter::convert()
{
	int scale,
		colBaseDivisor,
		rowBaseDivisor,
		baseDivisor,
		segmentWidth;
	int frameNum = -1;
	std::string inputFilename = this->inputFilename;
	std::string output_filename = this->outputFilename;
	std::string input_default_filename("pieska nie obchodzi.mp4");
	std::string output_default_filename("pieska nie obchodzi_output.mp4");
	Size videoSize;
	Mat image;

	VideoCapture inputVideo(inputFilename);
	if (!inputVideo.isOpened())
	{
		cout << "Could not open specified file: " << inputFilename << "Please make sure it's inside the app folder." << endl;
		cout << endl << "Press any key to exit.";
		waitKey(0);
		return;
	}

	videoSize = Size((int)inputVideo.get(CAP_PROP_FRAME_WIDTH),
		(int)inputVideo.get(CAP_PROP_FRAME_HEIGHT));

	baseDivisor = gcd(videoSize.width, videoSize.height);
	colBaseDivisor = videoSize.width / baseDivisor;
	rowBaseDivisor = videoSize.height / baseDivisor;

	scale = baseDivisor;
	segmentWidth = videoSize.width / colBaseDivisor / scale;

	for (; segmentWidth < 5;)
	{
		scale /= lcm(scale, 2 * 3 * 5 * 7 * 11 * 13 * 17);
		segmentWidth = videoSize.width / colBaseDivisor / scale;
	}

	VideoWriter outputVideo(
		outputFilename,
		inputVideo.get(CV_CAP_PROP_FOURCC),
		inputVideo.get(CV_CAP_PROP_FPS),
		Size(
			inputVideo.get(CV_CAP_PROP_FRAME_WIDTH),
			inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT)
		)
	);

	cout << "Video conversion in progress." << endl;

	this->exportVideo(inputVideo, outputVideo, scale, colBaseDivisor, rowBaseDivisor);
}


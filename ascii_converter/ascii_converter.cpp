// ascii_converter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "VideoToAsciiConverter.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <algorithm>
using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	int scale,
		colBaseDivisor,
		rowBaseDivisor,
		baseDivisor,
		segmentWidth;
	int frameNum = -1;
	string input_filename;
	string output_filename;
	string input_default_filename("pieska nie obchodzi.mp4");
	string output_default_filename("pieska nie obchodzi_output.mp4");
	Size videoSize;
	Mat videoFrame,
		image;

	cout << "Please place video in ascii_converter.exe folder and enter its name here (extension included)" << endl << "or leave empty for default video (piesek.mp4)" << endl;

	getline(cin, input_filename);
	if (input_filename.length() == 0) {
		input_filename = input_default_filename;
	}

	cout << "Please enter the output video filename" << endl << "or leave empty for default (pieska nie obchodzi_output.mp4)" << endl;

	getline(cin, output_filename);
	if (output_filename.length() == 0) {
		output_filename = output_default_filename;
	}

	VideoToAsciiConverter AsciiConverter(input_filename, output_filename);
	AsciiConverter.convert();

	cout << endl << "Press any key to exit.";
	//waitKey(0);
	return 0;
}

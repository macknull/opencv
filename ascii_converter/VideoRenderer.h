#pragma once
#include <opencv2/opencv.hpp>
class VideoRenderer
{
private:
	cv::VideoWriter output_cap;
public:
	VideoRenderer(std::string name, cv::VideoCapture& input_cap);
	~VideoRenderer();
	void write(cv::Mat frame);
	void release();
};


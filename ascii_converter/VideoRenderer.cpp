#include "stdafx.h"
#include "VideoRenderer.h"

VideoRenderer::VideoRenderer(std::string name, cv::VideoCapture& input_cap)
{
	output_cap.open(
			name,
			input_cap.get(CV_CAP_PROP_FOURCC),
			input_cap.get(CV_CAP_PROP_FPS),
			cv::Size(input_cap.get(CV_CAP_PROP_FRAME_WIDTH),
			input_cap.get(CV_CAP_PROP_FRAME_HEIGHT)));

	if (!output_cap.isOpened())
	{
		exit(-1);
	}
}

VideoRenderer::~VideoRenderer()
{
	output_cap.release();
}

void VideoRenderer::write(cv::Mat frame)
{
	output_cap.write(frame);
}
void VideoRenderer::release()
{
	output_cap.release();
}

#include "stdafx.h"
#include "AsciiOutput.h"


AsciiOutput::AsciiOutput(cv::Mat& m,int row_count, int row_height, int col_count, int col_width, int scale)
	:point(0, 0), 
	output(m), 
	row_count(row_count), 
	row_height(row_height), 
	col_count(col_count), 
	col_width(col_width),
	char_scale((double)4 / (double)scale)
{
	
}

AsciiOutput::~AsciiOutput()
{}

void AsciiOutput::fillFrame(std::string text, std::vector<cv::Scalar>& channels)
{
	point.x = 0;
	std::string temp;
	for (int col = 0; col < col_count; ++col)
	{
		point.y = col_width;
		for (int row = 0; row < row_count; ++row)
		{
			int index = col * row_count + row;
			temp = text[index];
			cv::Scalar segmentChannels = channels[index];
			cv::putText(output, temp, point, 2, char_scale, segmentChannels);
			point.y += col_width;
		}
		point.x += row_height;
		
	}
	
}

void AsciiOutput::makeFrame(std::vector<cv::Scalar>& channels)
{
	std::string chars;
	for (int i = 0; i < channels.size(); ++i)
	{
		char c = getASCII(channels[i][3]);
		chars.push_back(c);
	}
	fillFrame(chars, channels);
}

char AsciiOutput::getASCII(int brightness)
{
	return scale[brightness*(scale.length()-1)/255];
}




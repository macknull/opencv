#pragma once
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
class AsciiOutput
{
private:
	const std::string scale = " -_+<>i!lI?/|*(1{[LCJUYXZO0QWMB8&%$#@@";
	cv::Point point;
	cv::Mat output;

	void fillFrame(std::string text, std::vector<cv::Scalar>& channels);
	char getASCII(int brightness);

	int row_count;
	int row_height;
	int col_count;
	int col_width;
	double char_scale;

public:
	AsciiOutput(cv::Mat& m, int row_count, int height_size, int width, int col_width, int char_scale);
	~AsciiOutput();
	void makeFrame(std::vector<cv::Scalar>& channels);

};

